# TimeTracker Beta

# Stage 3
In this stage, we transfered our desktop app into Chrome extension. Please download two files for Chrome extension and deskyop background app seperately.

## Instructions for TimeTracker Stage 3

Please make sure every file are in the same folder.

### Setup
1. Open `Chrome` and go to `chrome://extensions/`.
2. Enable `Developer mode` in the right top, and `load unpacked` the folder `Chrome-Extension`.
3. Run `init.bat` by double clicking it.
4. Back to `Chrome` and reload the extension.

### Features
1. When `Chrome` is opened, the extension will run automatically, and you can find a tray icon at the right bottom. By double clicking it, you can exit the background app.
2. Click the extension icon, and click the third button which will lead you to the dashboard page.
3. The dashboard page will show current goal and related tasks.
4. The report page will show daily report and weekly report
5. The set goal page allow you to view and set goal.

## Test
In order to add data into indexDB, please click the forth button which leads you to `database.html`.

---

# Stage 2
## Instructions for TimeTracker Stage 2:
Our team highly appreciate your participation! 

### First, download and install our app TimeTracker
1. Downlaod project_37.msi.
2. Install and run the program. 
3. Use your computer as usual.

### Then, please follow the steps shown below:
1. Look at the menu
2. Click 'Home' - 'View your activities' to see your recorded activities
3. Click 'Goal' - 'Set Goals' to set a goal by clicking the drop down button (click 'OK' to confirm)
4. Click 'Goal' - 'View Goal' to view your current goal 
5. Close the 'View Goal' dialog

### Now, try to define your own goals
1. Click 'Goal' - 'Set Goals' to set a new goal
2. Click 'Add new' button in 'Set Goals' dialog to add a new goal
3. Input the name of the goal (whatever you want)
4. Click 'Add a task' button in 'Add a goal' dialog to add/personalize tasks for your new goals
5. Click "refresh" to check your newly added tasks/goals

### REMEMBER: Click 'OK' to confirm.
1. Reopen 'Goal' - 'Set Goals', your new goal is shown in the drop down button 
  
### Last!!!
Please fill in the survey for us :-).  
Thank you so much for your patience!  
[Look at me!](https://surveys.hotjar.com/s?siteId=1486330&surveyId=141950)

---

# Stage 1
This repo is aiming to gather test data for our app, thank you for your time and patience in advance!  
Please take a minute to fill out this [survey](https://www.surveymonkey.com/r/W78H7NH)

# Instruction
1. Downlaod project_37.msi.
2. Install and run the program. 
3. Use your computer as usual.
4. Click Home/View Statistics to show your app usage time.
5. Exit program.
6. Reopen program and do step 3 and 4 again.
7. Give us feedback

# Possible issues
1. If `CreateNewFile` error appears, please try to install it in your own defined directory (e.g. `desktop`) instead of default one.
2. It won't refresh the result by itself, you can firstly close statistic view window, and then do step 4.


